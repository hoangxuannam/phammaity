package zentech.learning1;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

public class Main extends Activity implements SensorEventListener {
    private SensorManager mSensorManager = null;
    DecimalFormat df = new DecimalFormat(".####");
    double []acce=new double[3];
    double []gra=new double[3];
    double []g=new double[3];
    double vertical_velocity = 0.0, H = 0.0;
    double t=0.0;
    private double lastUpdate;
    private double last_vertical_velocity = 0.0;
    // tạo tên file
    private String acc;
    // đường dẫn lưu file
    private final String filepath = "/mnt/sdcard/acc.txt";
    private BufferedWriter mBufferedWriter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mSensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);

    }
    //tao nut an Start
    public void onStartClick(View view)
    {
        initListeners();
    }
    //tao nut an Stop
    public void onStopClick(View view)
    {
        mSensorManager.unregisterListener(this);
        WriteFile(filepath, acc);
    }
    // tao file
    public void CreateFile(String path)
    {
        File f = new File(path);
        try {
            Log.d("ACTIVITY", "Create a File.");
            f.createNewFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    //ghi data
    public void WriteFile(String filepath, String str)
    {
        mBufferedWriter = null;

        if (!FileIsExist(filepath))
            CreateFile(filepath);

        try
        {
            mBufferedWriter = new BufferedWriter(new FileWriter(filepath, true));
            mBufferedWriter.write(str);
            mBufferedWriter.newLine();
            mBufferedWriter.flush();
            mBufferedWriter.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    //xet dieu kien file da tao chua
    public boolean FileIsExist(String filepath)
    {
        File f = new File(filepath);

        if (! f.exists())
        {
            Log.e("ACTIVITY", "File does not exist.");
            return false;
        }
        else
            return true;
    }
    //nhan tin hieu sensor
    public void initListeners() {
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_FASTEST);
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    //xu ly tin hieu
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_LINEAR_ACCELERATION:
                linear_acceleration_Function(event);
                vertical_acceleration_Function();
                break;
            case Sensor.TYPE_GRAVITY:
                gravity_Function(event);
                vertical_acceleration_Function();
                break;
        }
    }

    public void gravity_Function(SensorEvent event) {
        Sensor mySensor = event.sensor;
        if (mySensor.getType() == Sensor.TYPE_GRAVITY) {

            gra[0] = event.values[0];
            gra[1] = event.values[1];
            gra[2] = event.values[2];
        }
    }

    public void linear_acceleration_Function (SensorEvent event){
        Sensor mySensor = event.sensor;
        if (mySensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            acce[0] = event.values[0];
            acce[1] = event.values[1];
            acce[2] = event.values[2];
        }
    }

    public void vertical_acceleration_Function() {
        double vertical_acc;
        double curTime = System.currentTimeMillis();

        if ((curTime - lastUpdate) >= 15.0) {
            //tinh thoi gian 1 lan chay
            double diffTime = (curTime - lastUpdate) / 1000.0;
            lastUpdate = curTime;
            //dem so lan chay
            t = t + 1;
            //gia toc cua 3 phuong chieu len phuong thang dung
            g[0] = acce[0]*gra[0]/9.81;
            g[1] = acce[1]*gra[1]/9.81;
            g[2] = acce[2]*gra[2]/9.81;

            //tinh gia toc tac dung theo phuong thang dung
            vertical_acc = g[0] + g[1] + g[2];

            vertical_velocity += vertical_acc * diffTime;
            H += last_vertical_velocity * diffTime + vertical_acc * diffTime * diffTime / 2.0;
            last_vertical_velocity = vertical_velocity;

            TextView text = (TextView) findViewById(R.id.F);
            text.setText("diffTime: " + df.format(diffTime));
            text = (TextView) findViewById(R.id.V);
            text.setText("Velocity: " + df.format(vertical_velocity));
            text = (TextView) findViewById(R.id.H);
            text.setText("Height: " + df.format(H));

            acc = acc + String.valueOf(df.format(t)) + "\t" + String.valueOf(df.format(vertical_acc))
                    + "\t" + String.valueOf(df.format(g[0])) + "\t" + String.valueOf(df.format(g[1]))
                    + "\t" + String.valueOf(df.format(g[2]))  + "\t" + String.valueOf(df.format(vertical_velocity))
                    + "\t" + String.valueOf(df.format(H))+ "\n";


        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}